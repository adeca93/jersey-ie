package model;

import model.interfaces.IUser;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by Andrea on 23/10/2017.
 *
 * This class represents a simple User.
 * A user is composed from two fields: id, name.
 *
 */
@XmlRootElement
public class User implements IUser {

    private String id;
    private String name;

    public User(String id, String name){
        super();
        this.id = id;
        this.name = name;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public static IUser createUser(String name){
        UUID uuid = UUID.randomUUID();
        return new User(uuid.toString(), name);
    }

}
