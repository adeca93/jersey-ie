package model.interfaces;

/**
 * Created by Andrea on 23/10/2017.
 *
 * Interface for user.
 *
 */
public interface IUser {

    /**
     * Returns the ID of the user.
     * @return ID of user.
     */
    String getID();

    /**
     * Returns the name of the user.
     * @return Name of user.
     */
    String getName();

}
