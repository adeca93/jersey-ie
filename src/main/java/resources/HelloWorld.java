package resources;

import model.User;
import model.interfaces.IUser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Andrea on 23/10/2017.
 *
 */
@Path("helloworld")
public class HelloWorld {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(){
        return "hello";
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public String hiSomeone(@PathParam("name") String name){
        return "Hi " + name;
    }

    @GET
    @Path("/json/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public User hiSomeoneJson(@PathParam("name") String name){
        IUser user = User.createUser(name);
        return (User) user;
    }

}
